@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.spring.boot)
}

tasks.bootJar { enabled = false }
tasks.jar { enabled = true }

dependencies {
    api(project(":autoconfigure"))
}

publishing {
    publications {
        create<MavenPublication>("n2-cards-spring-boot-starter") {
            from(components["java"])
            groupId = "com.n2.cards"
            artifactId = "n2-cards-spring-boot-starter"
            version = "0.0.6"
        }
    }
}
