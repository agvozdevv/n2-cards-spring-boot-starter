rootProject.name = "n2-cards-spring-boot-starter"

include("autoconfigure", "starter")

enableFeaturePreview("VERSION_CATALOGS")

dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)

    repositories {
        mavenCentral()
        maven { url = uri("https://repo.spring.io/milestone") }
        maven { url = uri("https://repo.spring.io/snapshot") }
        maven {
            url = uri(
                "https://gitlab.com/api/v4/groups/13786379/-/packages/maven"
            )
            credentials(HttpHeaderCredentials::class.java) {
                val jobToken = System.getenv("CI_PRIVATE_JOB_TOKEN")
                if (jobToken != null) {
                    name = "Deploy-Token"
                    value = jobToken
                } else if (extra.has("gitLabPrivateToken")) {
                    name = "Private-Token"
                    value = (extra["gitLabPrivateToken"] as String)
                }
            }

            authentication {
                register("header", HttpHeaderAuthentication::class.java)
            }
        }
        mavenLocal()
    }

    versionCatalogs {
        create("libs") {
            from("com.parity:dependency-versions-module:0.0.4")
        }
    }
}

pluginManagement {
    repositories {
        maven { url = uri("https://repo.spring.io/milestone") }
        maven { url = uri("https://repo.spring.io/snapshot") }
        gradlePluginPortal()
    }
}
