import io.gitlab.arturbosch.detekt.Detekt
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    id("io.spring.dependency-management") version "1.0.11.RELEASE" apply false
    kotlin("jvm") version "1.6.21" apply false
    kotlin("plugin.spring") version "1.6.21" apply false
    kotlin("kapt") version "1.6.21" apply false
    alias(libs.plugins.spring.boot) apply false
    alias(libs.plugins.ktlint.detekt) apply false
}

subprojects {
    apply { plugin("java-library") }
    apply { plugin("maven-publish") }
    apply { plugin("io.spring.dependency-management") }
    apply { plugin("org.jetbrains.kotlin.jvm") }
    apply { plugin("org.jetbrains.kotlin.plugin.spring") }
    apply { plugin("org.jetbrains.kotlin.kapt") }

    apply { from("$rootDir/gradle/ktlint.gradle.kts") }
    apply { plugin("io.gitlab.arturbosch.detekt") }

    tasks.withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = JavaVersion.VERSION_16.toString()
        }
    }

    tasks.withType<Test> {
        useJUnitPlatform()
    }

    tasks.withType<Detekt> {
        exclude("resources/")
        exclude("build/")
        config.setFrom(files("$rootDir/gradle/detekt-config.yml"))
    }
}
