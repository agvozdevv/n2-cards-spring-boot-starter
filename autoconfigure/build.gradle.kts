@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.spring.boot)
}

tasks.bootJar { enabled = false }
tasks.jar { enabled = true }

dependencies {
    annotationProcessor("org.springframework.boot:spring-boot-autoconfigure-processor")

    api(kotlin("stdlib"))
    api(kotlin("reflect"))

    api("com.parity:error-handler-starter:0.0.1.4")
    api("com.n2:request-logger:0.0.13")

    api("org.springframework.boot:spring-boot-starter-webflux")
    api("org.springframework.boot:spring-boot-starter-validation")

    api("com.fasterxml.jackson.module:jackson-module-kotlin")
    api("io.projectreactor.kotlin:reactor-kotlin-extensions")
    api("org.mapstruct:mapstruct:1.4.2.Final")

    api("org.springframework.boot:spring-boot-starter-actuator")
    api("org.springframework.cloud:spring-cloud-sleuth-zipkin:3.0.4")
    api("io.springfox:springfox-boot-starter:3.0.0")
    api("io.micrometer:micrometer-registry-prometheus")

    api(libs.bundles.feign.client)
    api(libs.bundles.springdoc.openapi)
    api(libs.spring.cloud.starter.sleuth)
    api(libs.logstash.logback.encoder)

    api("org.springframework.boot:spring-boot-starter-test")
    api("io.projectreactor:reactor-test")
    api(libs.mockito.kotlin)
    api("org.mockito:mockito-inline")
    api("com.ninja-squad:springmockk:3.1.1")
}

publishing {
    publications {
        create<MavenPublication>("n2-cards-spring-boot-starter") {
            from(components["java"])
            groupId = "com.n2.cards"
            artifactId = "n2-cards-spring-boot-autoconfigure"
            version = "0.0.6"
        }
    }
}
