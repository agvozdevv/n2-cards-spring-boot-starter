package com.n2.cards.n2cardsspringbootstarter.config

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue
import com.fasterxml.jackson.databind.ObjectMapper
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.autoconfigure.AutoConfigurations
import org.springframework.boot.test.context.runner.ApplicationContextRunner

class ObjectMapperConfigTests {
    private val applicationContextRunner = ApplicationContextRunner()
        .withConfiguration(AutoConfigurations.of(ObjectMapperConfig::class.java))

    @Test
    fun `should test that map is sorted`() {
        applicationContextRunner.run {
            val objectMapper: ObjectMapper = it.getBean(ObjectMapper::class.java)

            val sortedValues = objectMapper.readValue(unsortedValues, HashMap::class.java)

            assertThat(it).hasSingleBean(ObjectMapper::class.java)
            assertThat(sortedValues.keys).containsExactly("1", "2", "3")
        }
    }

    @Test
    fun `should test that unknown enum value is mapped default value`() {
        applicationContextRunner.run {
            val objectMapper = it.getBean(ObjectMapper::class.java)

            val unknownValue = objectMapper.readValue("\"Y\"", LETTERS::class.java)

            assertThat(it).hasSingleBean(ObjectMapper::class.java)
            assertThat(unknownValue).isEqualTo(LETTERS.UNKNOWN)
        }
    }

    private val unsortedValues = """
        {
            "2": "2",
            "3": "3",
            "1": "1"
        }
    """.trimIndent()

    internal enum class LETTERS {
        A, B,

        @JsonEnumDefaultValue
        UNKNOWN
    }
}
