package com.n2.cards.n2cardsspringbootstarter.config

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.autoconfigure.AutoConfigurations
import org.springframework.boot.test.context.runner.ApplicationContextRunner
import java.util.TimeZone

class TimeZoneConfigTests {
    private val applicationContextRunner = ApplicationContextRunner()
        .withConfiguration(AutoConfigurations.of(TimeZoneConfig::class.java))

    @Test
    fun `should test time zone config set time zone`() {
        applicationContextRunner.run {
            assertThat(TimeZone.getDefault()).isEqualTo(TimeZone.getTimeZone("UTC"))
        }
    }
}
