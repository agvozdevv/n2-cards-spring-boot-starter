package com.n2.cards.n2cardsspringbootstarter.config

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.autoconfigure.AutoConfigurations
import org.springframework.boot.test.context.runner.ApplicationContextRunner

class MarqetaAuthConfigTests {
    private val applicationContextRunner = ApplicationContextRunner()
        .withConfiguration(AutoConfigurations.of(MarqetaAuthConfig::class.java))

    @Test
    fun `should test marqeta auth config is loaded when properties set`() {
        applicationContextRunner
            .withPropertyValues("marqeta.api.auth.user=username", "marqeta.api.auth.password=password")
            .run {
                assertThat(it).hasSingleBean(MarqetaAuthConfig::class.java)
            }
    }

    @Test
    fun `should test marqeta auth config is not loaded without username provided`() {
        applicationContextRunner
            .withPropertyValues("marqeta.api.auth.password=password")
            .run {
                assertThat(it).doesNotHaveBean(MarqetaAuthConfig::class.java)
            }
    }

    @Test
    fun `should test marqeta auth config is not loaded without password provided`() {
        applicationContextRunner
            .withPropertyValues("marqeta.api.auth.user=username")
            .run {
                assertThat(it).doesNotHaveBean(MarqetaAuthConfig::class.java)
            }
    }
}
