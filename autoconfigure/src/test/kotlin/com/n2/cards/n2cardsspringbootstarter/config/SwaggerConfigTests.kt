package com.n2.cards.n2cardsspringbootstarter.config

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.autoconfigure.AutoConfigurations
import org.springframework.boot.test.context.runner.ApplicationContextRunner

class SwaggerConfigTests {
    private val applicationContextRunner = ApplicationContextRunner()
        .withConfiguration(AutoConfigurations.of(SwaggerConfig::class.java))

    @Test
    fun `should test swagger config is loaded when properties set`() {
        applicationContextRunner
            .withPropertyValues("swagger.api.title=title", "swagger.api.description=description")
            .run {
                assertThat(it).hasSingleBean(SwaggerConfig::class.java)
            }
    }

    @Test
    fun `should test swagger config is not loaded without title provided`() {
        applicationContextRunner
            .withPropertyValues("swagger.api.description=description")
            .run {
                assertThat(it).doesNotHaveBean(SwaggerConfig::class.java)
            }
    }

    @Test
    fun `should test swagger config is not loaded without description provided`() {
        applicationContextRunner
            .withPropertyValues("swagger.api.title=title")
            .run {
                assertThat(it).doesNotHaveBean(SwaggerConfig::class.java)
            }
    }
}
